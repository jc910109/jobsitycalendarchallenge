const cities = require('./citiesOriginal.json');
const fs = require('fs');
var path = require('path');
const countryCodes = require('country-codes-list');

const countryCodesData = countryCodes.customList(
  'countryCode',
  '{countryNameEn}',
);
const countryList = Object.keys(countryCodesData).map((code) => ({
  code,
  name: countryCodesData[code],
}));

const countryCitiesMap = {};

for (let index = 0; index < countryList.length; index++) {
  const country = countryList[index];
  countryCitiesMap[country.code] = [];
}
for (let index = 0; index < cities.length; index++) {
  const city = cities[index];
  if (countryCitiesMap[city.country]) {
    countryCitiesMap[city.country].push({
      id: city.id,
      name: city.name,
    });
  }
}
for (let index = 0; index < countryList.length; index++) {
  const country = countryList[index];
  countryCitiesMap[country.code].sort((a, b) => a.name.localeCompare(b.name));
}

var jsonPath = path.join(__dirname, 'cities.json');
fs.writeFile(jsonPath, JSON.stringify(countryCitiesMap), (err, result) => {
  if (err) {
    console.log('error', err);
  }
});
