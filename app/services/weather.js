import axios from 'axios';
import {apiUrl, apiKey} from '../config/weather';
import {getUnixTimestamp} from '../helpers/date';

const axiosCLient = axios.create({
  baseURL: apiUrl,
});

axiosCLient.interceptors.request.use((config) => {
  config.params = config.params || {};
  config.params.appid = apiKey;
  return config;
});

export const getWeatherFromDateTime = async (cityId, date, time) => {
  const result = await axiosCLient.get('forecast', {
    params: {
      id: cityId,
    },
  });
  const data = result.data;
  if (
    data.list &&
    data.list.length > 0 &&
    data.list[0].weather &&
    data.list[0].weather.length > 0
  ) {
    const dateTimeUnix = getUnixTimestamp(date, time);
    let weather = data.list[0].weather[0];
    let minDiff = Math.abs(dateTimeUnix - data.list[0].dt);
    for (let index = 1; index < data.list.length; index++) {
      const elem = data.list[index];
      const diff = Math.abs(dateTimeUnix - elem.dt);
      if (diff < minDiff) {
        minDiff = diff;
        weather = elem.weather[0];
      }
    }
    return weather;
  }
  return null;
};
