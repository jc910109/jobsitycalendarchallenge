import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Calendar from '../components/screens/Calendar';
import ReminderList from '../components/screens/Reminder/ReminderList';
import ReminderAddEdit from '../components/screens/Reminder/ReminderAddEdit';
import {navigationRef} from '../helpers/navigation';

const Stack = createStackNavigator();

const Navigation = () => {
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator
        screenOptions={{
          headerTitleAlign: 'center',
        }}>
        <Stack.Screen
          name="Calendar"
          component={Calendar}
          options={{
            title: 'My Calendar',
          }}
        />
        <Stack.Screen
          name="ReminderList"
          component={ReminderList}
          options={({route}) => ({
            title: `Reminders from ${route.params.date}`,
          })}
        />
        <Stack.Screen
          name="ReminderAddEdit"
          component={ReminderAddEdit}
          options={({route}) => ({
            title: `${route.params.reminder ? 'Edit' : 'Add'} Reminder ${
              route.params.reminder ? 'from' : 'to'
            } ${route.params.date}`,
          })}
        />
        <Stack.Screen
          name="DatePicker"
          component={Calendar}
          options={{
            title: 'Pick a Date',
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigation;
