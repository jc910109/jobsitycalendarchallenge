export const ScreenSizes = {
  SMALL: 'small',
  MEDIUM: 'medium',
  LARGE: 'large',
};

export const Theme = {
  screenSizes: {
    small: 450,
    medium: 600,
    large: 750,
  },
};
