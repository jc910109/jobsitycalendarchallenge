export default {
  calendar: {
    weekDays: [
      {
        shortTitle: 'Sun',
        title: 'Sunday',
      },
      {
        shortTitle: 'Mon',
        title: 'Monday',
      },
      {
        shortTitle: 'Tue',
        title: 'Tuesday',
      },
      {
        shortTitle: 'Wed',
        title: 'Wednesday',
      },
      {
        shortTitle: 'Thu',
        title: 'Thursday',
      },
      {
        shortTitle: 'Fri',
        title: 'Friday',
      },
      {
        shortTitle: 'Sat',
        title: 'Saturday',
      },
    ],
    months: [
      {
        title: 'January',
      },
      {
        title: 'February',
      },
      {
        title: 'March',
      },
      {
        title: 'April',
      },
      {
        title: 'May',
      },
      {
        title: 'June',
      },
      {
        title: 'July',
      },
      {
        title: 'August',
      },
      {
        title: 'September',
      },
      {
        title: 'October',
      },
      {
        title: 'November',
      },
      {
        title: 'December',
      },
    ],
  },
};
