export const Colors = {
  blue: '#2C74B9',
  white: '#ffffff',
  lightGrey: '#F2F2F2',
  grey: '#A6A6A6',
  black: '#000000',
  red: '#FF0000',
};

export const PickerColors = [
  {
    code: 'black',
  },
  {
    code: 'red',
  },
  {
    code: 'green',
  },
  {
    code: 'blue',
  },
  {
    code: 'yellow',
  },
  {
    code: 'lightblue',
  },
  {
    code: 'pink',
  },
  {
    code: 'grey',
  },
  {
    code: 'darkred',
  },
  {
    code: 'orange',
  },
  {
    code: 'purple',
  },
  {
    code: 'cyan',
  },
  {
    code: 'magenta',
  },
  {
    code: 'darkgreen',
  },
  {
    code: 'brown',
  },
  {
    code: 'darkgrey',
  },
  {
    code: 'darkblue',
  },
  {
    code: 'gold',
  },
];
