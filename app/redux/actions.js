import {
  SET_CALENDAR_MONTH,
  SET_CALENDAR_YEAR,
  ADD_REMINDER,
  EDIT_REMINDER,
  DELETE_REMINDER,
  DELETE_ALL_REMINDERS,
} from './actionTypes';

let nextReminderId = 0;

export const setCalendarMonth = (newMonthIndex) => ({
  type: SET_CALENDAR_MONTH,
  payload: {
    value: newMonthIndex,
  },
});

export const setCalendarYear = (newYear) => ({
  type: SET_CALENDAR_YEAR,
  payload: {
    value: newYear,
  },
});

export const addReminder = (content) => ({
  type: ADD_REMINDER,
  payload: {
    id: ++nextReminderId,
    content,
  },
});

export const editReminder = (content) => ({
  type: EDIT_REMINDER,
  payload: {
    id: content.id,
    content,
  },
});

export const deleteReminder = (content) => ({
  type: DELETE_REMINDER,
  payload: {
    id: content.id,
    content,
  },
});

export const deleteAllReminders = (date) => ({
  type: DELETE_ALL_REMINDERS,
  payload: {
    date,
  },
});
