import {combineReducers} from 'redux';
import calendar from './calendar';
import reminder from './reminder';

export default combineReducers({calendar, reminder});
