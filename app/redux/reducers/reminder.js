import {
  ADD_REMINDER,
  EDIT_REMINDER,
  DELETE_REMINDER,
  DELETE_ALL_REMINDERS,
} from '../actionTypes';

const initialState = {
  reminders: {},
};

export default function (state = initialState, action) {
  switch (action.type) {
    case ADD_REMINDER: {
      const {id, content} = action.payload;
      delete content.initialDate;
      const {reminders} = state;
      if (!reminders[content.date]) {
        reminders[content.date] = [];
      }
      reminders[content.date].push({
        ...content,
        id,
      });
      return {
        reminders: {
          ...reminders,
        },
      };
    }
    case EDIT_REMINDER: {
      const {id, content} = action.payload;
      const initialDate = content.initialDate;
      delete content.initialDate;
      const {reminders} = state;
      if (!reminders[content.date]) {
        reminders[content.date] = [
          {
            id: content.id,
          },
        ];
      }
      if (reminders[content.date]) {
        for (let index = 0; index < reminders[content.date].length; index++) {
          const currentReminder = reminders[content.date][index];
          if (currentReminder.id === id) {
            reminders[content.date][index] = content;
          }
        }
      }
      if (initialDate !== content.date && reminders[initialDate]) {
        for (let index = 0; index < reminders[initialDate].length; index++) {
          const currentReminder = reminders[initialDate][index];
          if (currentReminder.id === id) {
            reminders[initialDate].splice(index, 1);
          }
        }
      }
      return {
        reminders: {
          ...reminders,
        },
      };
    }
    case DELETE_REMINDER: {
      const {id, content} = action.payload;
      const {reminders} = state;
      if (reminders[content.date]) {
        for (let index = 0; index < reminders[content.date].length; index++) {
          const currentReminder = reminders[content.date][index];
          if (currentReminder.id === id) {
            reminders[content.date].splice(index, 1);
          }
        }
      }
      return {
        reminders: {
          ...reminders,
        },
      };
    }
    case DELETE_ALL_REMINDERS: {
      const {date} = action.payload;
      const {reminders} = state;
      if (reminders[date]) {
        delete reminders[date];
      }
      return {
        reminders: {
          ...reminders,
        },
      };
    }
    default:
      return state;
  }
}
