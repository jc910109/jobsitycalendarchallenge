import {SET_CALENDAR_YEAR, SET_CALENDAR_MONTH} from '../actionTypes';
import {getCurrentDate} from '../../helpers/date';

const currentDate = getCurrentDate();

const initialState = {
  currentYear: currentDate.getFullYear(),
  currentMonthIndex: currentDate.getMonth(),
};

const calendar = (state = initialState, action) => {
  switch (action.type) {
    case SET_CALENDAR_YEAR: {
      return {
        ...state,
        currentYear: action.payload.value,
      };
    }
    case SET_CALENDAR_MONTH: {
      return {
        ...state,
        currentMonthIndex: action.payload.value,
      };
    }
    default: {
      return state;
    }
  }
};

export default calendar;
