import moment from 'moment';

export const getCurrentDate = () => {
  return new Date();
};

export const formatDate = (date) => {
  return moment(date).format('DD/MM/YYYY');
};

export const formatTime = (time) => {
  return moment(time).format('HH:mm');
};

export const getFirstDateMonth = (year, monthIndex) => {
  return moment(`${year}/${monthIndex + 1}`, 'YYYY/M')
    .startOf('month')
    .toDate();
};

export const getLastDateMonth = (year, monthIndex) => {
  return moment(`${year}/${monthIndex + 1}`, 'YYYY/M')
    .endOf('month')
    .toDate();
};

export const getFirstDateWeek = (date) => {
  return moment(date).startOf('week').toDate();
};

export const getLastDateWeek = (date) => {
  return moment(date).endOf('week').toDate();
};

export const getDatesForCalendar = (year, monthIndex) => {
  let dates = [];
  const firstMonthDate = getFirstDateMonth(year, monthIndex);
  const lastMonthDate = getLastDateMonth(year, monthIndex);
  const firstDateWeek = getFirstDateWeek(firstMonthDate);
  const lastDateWeek = getLastDateWeek(lastMonthDate);
  let currentDate = new Date(firstDateWeek.getTime());
  while (
    moment(currentDate).format('YYYY/MM/DD') <=
    moment(lastDateWeek).format('YYYY/MM/DD')
  ) {
    dates.push(currentDate);
    currentDate = moment(currentDate).add(1, 'day').toDate();
  }
  return dates;
};

export const checkDateWeatherAvailable = (date) => {
  const currentDate = moment(getCurrentDate()).format('YYYY/MM/DD');
  const nextFiveDays = moment(getCurrentDate())
    .add(5, 'days')
    .format('YYYY/MM/DD');
  const momentDate = moment(date, 'DD/MM/YYYY').format('YYYY/MM/DD');
  return momentDate >= currentDate && momentDate <= nextFiveDays;
};

export const getUnixTimestamp = (date, time) => {
  return parseInt(
    moment(`${date} ${time}`, 'DD/MM/YYYY HH:mm').format('X'),
    10,
  );
};

export const getDateObject = (date, time) => {
  return moment(`${date} ${time}`, 'DD/MM/YYYY HH:mm').toDate();
};
