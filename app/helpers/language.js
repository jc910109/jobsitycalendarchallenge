import language from '../config/language';
import {getScreenSize} from './theme';
import {ScreenSizes} from '../config/theme';

export const getWeekDayTitle = (weekDayIndex) => {
  const weekDayLanguage = language.calendar.weekDays[weekDayIndex];
  const screenSize = getScreenSize();
  switch (screenSize) {
    case ScreenSizes.SMALL:
      return weekDayLanguage.shortTitle;

    default:
      return weekDayLanguage.title;
  }
};

export const getMonthTitle = (monthIndex) => {
  const monthLanguage = language.calendar.months[monthIndex];
  return monthLanguage.title;
};
