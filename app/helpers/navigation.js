import * as React from 'react';

export const navigationRef = React.createRef();

export const navigation = {
  navigate: (name, params) => {
    if (navigationRef.current) {
      navigationRef.current.navigate(name, params);
    }
  },
  goBack: () => {
    if (navigationRef.current) {
      navigationRef.current.goBack();
    }
  },
};
