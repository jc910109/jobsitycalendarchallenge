import {Dimensions} from 'react-native';
import {Theme, ScreenSizes} from '../config/theme';

export const windowWidth = Dimensions.get('window').width;

export const getCellDimension = () => {
  const width = windowWidth;
  return width / 7;
};

export const getScreenSize = () => {
  if (windowWidth < Theme.screenSizes.small) {
    return ScreenSizes.SMALL;
  }
};

export const getColorSquareCount = () => {
  return Math.floor((windowWidth - 50) / 40);
};

export const getCalendarRemindersCount = () => {
  return Math.floor((getCellDimension() - 20) / 10);
};
