import countryCodes from 'country-codes-list';
import cities from '../static_data/cities.json';

export const chunk = (array, size) => {
  const chunked_arr = [];
  let copied = [...array];
  const numOfChild = Math.ceil(copied.length / size);
  for (let i = 0; i < numOfChild; i++) {
    chunked_arr.push(copied.splice(0, size));
  }
  return chunked_arr;
};

const countryCodesData = countryCodes.customList(
  'countryCode',
  '{countryNameEn}',
);
const countryListInner = Object.keys(countryCodesData).map((code) => ({
  code,
  name: countryCodesData[code],
}));
countryListInner.sort((a, b) => a.name.localeCompare(b.name));

export const countryList = countryListInner;

export const getCitiesFromCountry = (countryCode) => {
  return cities[countryCode];
};

export const getCityName = (countryCode, cityId) => {
  return cities[countryCode].filter((city) => city.id === cityId).pop().name;
};

export const getCountryName = (countryCode) => {
  return countryListInner
    .filter((country) => country.code === countryCode)
    .pop().name;
};
