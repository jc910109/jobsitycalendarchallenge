import {StyleSheet} from 'react-native';
import {FontSizes} from '../../../../../config/fontSize';
import {Colors} from '../../../../../config/color';

export default StyleSheet.create({
  container: {},
  yearContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 10,
    paddingBottom: 10,
    borderBottomWidth: 0.5,
    borderBottomColor: Colors.grey,
  },
  currentYear: {
    fontSize: FontSizes.h1,
  },
  changeYearContainer: {
    marginHorizontal: 20,
    width: 30,
    height: 30,
    backgroundColor: Colors.lightGrey,
    borderColor: Colors.grey,
    borderWidth: 1,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  changeYear: {
    fontSize: FontSizes.h1,
  },
  monthContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 10,
  },
  currentMonth: {
    fontSize: FontSizes.h2,
  },
  changeMonthContainer: {
    marginHorizontal: 20,
    width: 30,
    height: 30,
    backgroundColor: Colors.lightGrey,
    borderColor: Colors.grey,
    borderWidth: 1,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  changeMonth: {
    fontSize: FontSizes.h2,
  },
});
