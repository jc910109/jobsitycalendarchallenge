import React from 'react';
import {View, Text, TouchableWithoutFeedback} from 'react-native';
import styles from './styles';
import {getMonthTitle} from '../../../../../helpers/language';
import {connect} from 'react-redux';
import {
  setCalendarYear as setCalendarYearAction,
  setCalendarMonth as setCalendarMonthAction,
} from '../../../../../redux/actions';

const CalendarHeader = ({
  currentYear,
  currentMonthIndex,
  setCalendarYear,
  setCalendarMonth,
}) => (
  <View style={styles.container}>
    <View style={styles.yearContainer}>
      <TouchableWithoutFeedback
        onPress={() => {
          setCalendarYear(currentYear - 1);
        }}>
        <View style={styles.changeYearContainer}>
          <Text style={styles.changeYear}>{'<'}</Text>
        </View>
      </TouchableWithoutFeedback>
      <Text style={styles.currentYear}>{currentYear}</Text>
      <TouchableWithoutFeedback
        onPress={() => {
          setCalendarYear(currentYear + 1);
        }}>
        <View style={styles.changeYearContainer}>
          <Text style={styles.changeYear}>{'>'}</Text>
        </View>
      </TouchableWithoutFeedback>
    </View>
    <View style={styles.monthContainer}>
      <TouchableWithoutFeedback
        onPress={() => {
          let newMonthIndex = currentMonthIndex - 1;
          if (newMonthIndex < 0) {
            newMonthIndex = 11;
            setCalendarYear(currentYear - 1);
          }
          setCalendarMonth(newMonthIndex);
        }}>
        <View style={styles.changeMonthContainer}>
          <Text style={styles.changeMonth}>{'<'}</Text>
        </View>
      </TouchableWithoutFeedback>
      <Text style={styles.currentMonth}>
        {getMonthTitle(currentMonthIndex)}
      </Text>
      <TouchableWithoutFeedback
        onPress={() => {
          let newMonthIndex = currentMonthIndex + 1;
          if (newMonthIndex > 11) {
            newMonthIndex = 0;
            setCalendarYear(currentYear + 1);
          }
          setCalendarMonth(newMonthIndex);
        }}>
        <View style={styles.changeMonthContainer}>
          <Text style={styles.changeMonth}>{'>'}</Text>
        </View>
      </TouchableWithoutFeedback>
    </View>
  </View>
);

const mapStateToProps = (state) => {
  const {calendar} = state;
  return {
    currentYear: calendar.currentYear,
    currentMonthIndex: calendar.currentMonthIndex,
  };
};

export default connect(mapStateToProps, {
  setCalendarYear: setCalendarYearAction,
  setCalendarMonth: setCalendarMonthAction,
})(CalendarHeader);
