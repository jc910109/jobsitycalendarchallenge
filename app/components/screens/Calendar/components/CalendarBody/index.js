import React from 'react';
import {View} from 'react-native';
import styles from './styles';
import CalendarBodyWeekDay from './components/CalendarBodyWeekDay';
import CalendarBodyDate from './components/CalendarBodyDate';
import {getDatesForCalendar, formatDate} from '../../../../../helpers/date';
import {chunk} from '../../../../../helpers/general';
import {connect} from 'react-redux';

class CalendarBody extends React.Component {
  render() {
    const {
      currentYear,
      currentMonthIndex,
      reminders,
      mode,
      onDateSelected,
    } = this.props;
    const dates = getDatesForCalendar(currentYear, currentMonthIndex);

    return (
      <View style={styles.container}>
        <View style={styles.headerRow}>
          {[...Array(7).keys()].map((weekDayIndex) => (
            <CalendarBodyWeekDay
              key={weekDayIndex}
              weekDayIndex={weekDayIndex}
            />
          ))}
        </View>
        {chunk(dates, 7).map((row, rowIndex) => (
          <View key={rowIndex} style={styles.bodyRow}>
            {row.map((date, dateIndex) => (
              <CalendarBodyDate
                key={dateIndex}
                date={date}
                reminders={
                  reminders[formatDate(date)]
                    ? reminders[formatDate(date)].sort((a, b) =>
                        a.time.localeCompare(b.time),
                      )
                    : []
                }
                mode={mode}
                onDateSelected={onDateSelected}
              />
            ))}
          </View>
        ))}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {calendar, reminder} = state;
  return {
    currentYear: calendar.currentYear,
    currentMonthIndex: calendar.currentMonthIndex,
    reminders: reminder.reminders,
  };
};

export default connect(mapStateToProps)(CalendarBody);
