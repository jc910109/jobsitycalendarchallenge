import {StyleSheet} from 'react-native';
import {getCellDimension} from '../../../../../../../helpers/theme';
import {Colors} from '../../../../../../../config/color';
import {FontSizes} from '../../../../../../../config/fontSize';

export default StyleSheet.create({
  container: {
    width: getCellDimension(),
    height: getCellDimension(),
    backgroundColor: Colors.white,
    paddingVertical: 2,
    paddingHorizontal: 5,
    borderLeftWidth: 1,
    borderLeftColor: Colors.grey,
  },
  containerWeekEnd: {
    backgroundColor: Colors.lightGrey,
  },
  dayName: {
    textAlign: 'left',
    color: Colors.black,
    fontSize: FontSizes.h4,
  },
  dayNameWeekEnd: {
    color: Colors.blue,
  },
  dayNameOtherMonth: {
    color: Colors.grey,
  },
  remindersContainer: {},
  reminder: {
    borderBottomWidth: 2,
    marginTop: 3,
  },
  more: {
    fontSize: FontSizes.h6,
    color: Colors.grey,
    textAlign: 'right',
  },
});
