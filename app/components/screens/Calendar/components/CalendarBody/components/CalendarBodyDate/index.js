import React from 'react';
import {View, Text} from 'react-native';
import styles from './styles';
import {connect} from 'react-redux';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {navigation} from '../../../../../../../helpers/navigation';
import {formatDate} from '../../../../../../../helpers/date';
import {getCalendarRemindersCount} from '../../../../../../../helpers/theme';
import {SELECT_DATE} from '../../../../constants/modes';

const CalendarBodyDate = ({
  currentMonthIndex,
  date,
  reminders,
  mode,
  onDateSelected,
}) => {
  let containerStyle = [styles.container];
  let dayStyle = [styles.dayName];

  const dayWeekIndex = date.getDay();
  if (dayWeekIndex === 0 || dayWeekIndex === 6) {
    containerStyle.push(styles.containerWeekEnd);
    dayStyle.push(styles.dayNameWeekEnd);
  }

  const dateMonthIndex = date.getMonth();
  if (dateMonthIndex !== currentMonthIndex) {
    dayStyle.push(styles.dayNameOtherMonth);
  }

  const remindersCount = getCalendarRemindersCount();
  return (
    <TouchableWithoutFeedback
      onPress={() => {
        if (mode === SELECT_DATE) {
          onDateSelected(formatDate(date));
        } else {
          navigation.navigate('ReminderList', {date: formatDate(date)});
        }
      }}>
      <View style={containerStyle}>
        <Text style={dayStyle}>{date.getDate()}</Text>
        <View style={styles.remindersContainer}>
          {reminders.slice(0, remindersCount).map((reminder) => (
            <View
              key={reminder.id}
              style={[styles.reminder, {borderBottomColor: reminder.color}]}
            />
          ))}
          {reminders.length > remindersCount && (
            <Text style={styles.more}>
              {reminders.length - remindersCount} More
            </Text>
          )}
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

const mapStateToProps = (state) => {
  const {calendar} = state;
  return {
    currentMonthIndex: calendar.currentMonthIndex,
  };
};

export default connect(mapStateToProps)(CalendarBodyDate);
