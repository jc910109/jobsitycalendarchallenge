import {StyleSheet} from 'react-native';
import {getCellDimension} from '../../../../../../../helpers/theme';
import {Colors} from '../../../../../../../config/color';
import {FontSizes} from '../../../../../../../config/fontSize';

export default StyleSheet.create({
  container: {
    width: getCellDimension(),
    backgroundColor: Colors.blue,
    paddingVertical: 5,
  },
  dayName: {
    textAlign: 'center',
    color: Colors.white,
    fontSize: FontSizes.h2,
  },
});
