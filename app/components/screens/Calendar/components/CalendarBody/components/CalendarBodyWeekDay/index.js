import React from 'react';
import {View, Text} from 'react-native';
import styles from './styles';
import {getWeekDayTitle} from '../../../../../../../helpers/language';

const CalendarBodyWeekDay = ({weekDayIndex}) => (
  <View style={styles.container}>
    <Text style={styles.dayName}>{getWeekDayTitle(weekDayIndex)}</Text>
  </View>
);

export default CalendarBodyWeekDay;
