import {StyleSheet} from 'react-native';
import {Colors} from '../../../../../config/color';

export default StyleSheet.create({
  container: {
    paddingTop: 10,
  },
  headerRow: {
    flexDirection: 'row',
  },
  bodyRow: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: Colors.grey,
  },
});
