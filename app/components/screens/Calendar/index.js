import React from 'react';
import {View, ScrollView} from 'react-native';
import styles from './styles';
import CalendarHeader from './components/CalendarHeader';
import CalendarBody from './components/CalendarBody';
import {SELECT_DATE} from './constants/modes';
import {navigation} from '../../../helpers/navigation';

class Calendar extends React.Component {
  render() {
    const {route} = this.props;
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.header}>
            <CalendarHeader />
          </View>
          <View style={styles.header}>
            <CalendarBody
              mode={route.params && route.params.mode ? route.params.mode : ''}
              onDateSelected={
                route.params && route.params.mode === SELECT_DATE
                  ? (date) => {
                      route.params.onDateSelected(date);
                      navigation.goBack();
                    }
                  : null
              }
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default Calendar;
