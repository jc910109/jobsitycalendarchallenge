import {StyleSheet} from 'react-native';
import {FontSizes} from '../../../../config/fontSize';
import {Colors} from '../../../../config/color';

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
  },
  buttonContainer: {
    paddingVertical: 10,
  },
  header: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  listEmptyContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  listEmpty: {
    fontSize: FontSizes.h4,
    color: Colors.grey,
  },
  listContentContainer: {
    flexGrow: 1,
  },
});
