import {StyleSheet} from 'react-native';
import {FontSizes} from '../../../../../../../../config/fontSize';
import {Colors} from '../../../../../../../../config/color';

export default StyleSheet.create({
  container: {},
  image: {
    width: 50,
    height: 50,
    alignSelf: 'flex-end',
  },
  notAvailable: {
    fontSize: FontSizes.h6,
    color: Colors.grey,
    textAlign: 'right',
  },
  location: {
    fontSize: FontSizes.h5,
    color: Colors.blue,
    textAlign: 'right',
  },
  weather: {
    fontSize: FontSizes.h6,
    color: Colors.grey,
    textAlign: 'right',
  },
});
