import React, {useEffect, useState} from 'react';
import {getWeatherFromDateTime} from '../../../../../../../../services/weather';
import {View, Text, Image, ActivityIndicator} from 'react-native';
import styles from './styles';
import {iconsUrl} from '../../../../../../../../config/weather';
import {checkDateWeatherAvailable} from '../../../../../../../../helpers/date';
import {
  getCityName,
  getCountryName,
} from '../../../../../../../../helpers/general';

const WeatherDateTimeCity = ({countryCity, date, time}) => {
  const [loading, setLoading] = useState(true);
  const [weather, setWeather] = useState(null);

  useEffect(() => {
    const getWeather = async () => {
      if (checkDateWeatherAvailable(date)) {
        const weatherResult = await getWeatherFromDateTime(
          countryCity.city,
          date,
          time,
        );
        setWeather(weatherResult);
      }
      setLoading(false);
    };
    getWeather();
    return () => {};
  }, [countryCity.country, countryCity.city, date, time]);
  return (
    <View style={styles.container}>
      {!loading ? (
        <View>
          {weather ? (
            <View>
              <Image
                style={styles.image}
                source={{
                  uri: iconsUrl + weather.icon + '@2x.png',
                }}
              />
              <Text style={styles.weather}>
                {weather.main} / {weather.description}
              </Text>
            </View>
          ) : (
            <Text style={styles.notAvailable}>Weather not available</Text>
          )}
          <Text style={styles.location}>
            {getCityName(countryCity.country, countryCity.city)} /{' '}
            {getCountryName(countryCity.country)}
          </Text>
        </View>
      ) : (
        <ActivityIndicator />
      )}
    </View>
  );
};

export default WeatherDateTimeCity;
