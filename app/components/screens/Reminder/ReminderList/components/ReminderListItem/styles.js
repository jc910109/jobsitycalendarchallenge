import {StyleSheet} from 'react-native';
import {Colors} from '../../../../../../config/color';
import {FontSizes} from '../../../../../../config/fontSize';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    marginTop: 10,
    borderWidth: 2,
    borderColor: Colors.black,
    borderRadius: 3,
    padding: 5,
    backgroundColor: Colors.white,
  },
  title: {
    fontSize: FontSizes.h2,
    fontWeight: 'bold',
    flex: 1,
    flexWrap: 'wrap',
  },
  time: {
    fontSize: FontSizes.h5,
    color: Colors.grey,
    textAlign: 'right',
  },
  deleteContainer: {
    marginTop: 10,
    width: '100%',
    backgroundColor: Colors.grey,
    borderRadius: 3,
    height: '100%',
    justifyContent: 'center',
  },
  deleteText: {
    fontSize: FontSizes.h1,
    color: Colors.white,
  },
});
