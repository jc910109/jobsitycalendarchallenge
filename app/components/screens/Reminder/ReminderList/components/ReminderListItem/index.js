import React from 'react';
import {View, Text, Button} from 'react-native';
import styles from './styles';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {navigation} from '../../../../../../helpers/navigation';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import {Colors} from '../../../../../../config/color';
import {connect} from 'react-redux';
import {deleteReminder as deleteReminderAction} from '../../../../../../redux/actions';
import WeatherDateTimeCity from './components/WeatherDateTimeCity';

const DeleteAction = ({onDeleteReminder}) => (
  <View style={styles.deleteContainer}>
    <Button
      title={'Tap to Delete'}
      onPress={() => {
        onDeleteReminder();
      }}
      color={Colors.red}
    />
  </View>
);

const ReminderListItem = ({reminder, deleteReminder}) => (
  <Swipeable
    renderLeftActions={() => (
      <DeleteAction
        onDeleteReminder={() => {
          deleteReminder(reminder);
        }}
      />
    )}>
    <TouchableWithoutFeedback
      onPress={() => {
        navigation.navigate('ReminderAddEdit', {reminder, date: reminder.date});
      }}>
      <View style={[styles.container, {borderColor: reminder.color}]}>
        <Text style={[styles.title, {color: reminder.color}]}>
          {reminder.title}
        </Text>
        <View>
          <WeatherDateTimeCity
            countryCity={reminder.countryCity}
            date={reminder.date}
            time={reminder.time}
          />
          <Text style={styles.time}>{reminder.time}</Text>
        </View>
      </View>
    </TouchableWithoutFeedback>
  </Swipeable>
);

export default connect(null, {
  deleteReminder: deleteReminderAction,
})(ReminderListItem);
