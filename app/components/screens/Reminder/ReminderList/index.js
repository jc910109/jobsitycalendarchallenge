import React from 'react';
import {View, Alert, Button, FlatList, Text} from 'react-native';
import styles from './styles';
import {navigation} from '../../../../helpers/navigation';
import ReminderListItem from './components/ReminderListItem';
import {deleteAllReminders as deleteAllRemindersAction} from '../../../../redux/actions';
import {connect} from 'react-redux';

class ReminderList extends React.Component {
  render() {
    const {route, reminders, deleteAllReminders} = this.props;

    const dateReminders = reminders[route.params.date]
      ? reminders[route.params.date]
      : [];
    dateReminders.sort((a, b) => a.time.localeCompare(b.time));
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          {dateReminders.length > 0 && (
            <Button
              title={'Delete All Reminders'}
              onPress={() => {
                Alert.alert(
                  'Delete All',
                  'Are you sure you want to delete every reminder from ' +
                    route.params.date,
                  [
                    {
                      text: 'Cancel',
                      onPress: () => {},
                      style: 'cancel',
                    },
                    {
                      text: 'OK',
                      onPress: () => {
                        deleteAllReminders(route.params.date);
                      },
                    },
                  ],
                  {cancelable: false},
                );
                //
              }}
            />
          )}
        </View>
        <FlatList
          data={dateReminders}
          renderItem={({item}) => <ReminderListItem reminder={item} />}
          keyExtractor={(item) => item.id.toString()}
          contentContainerStyle={styles.listContentContainer}
          ListEmptyComponent={() => (
            <View style={styles.listEmptyContainer}>
              <Text style={styles.listEmpty}>Reminders List Empty</Text>
            </View>
          )}
        />
        <View style={styles.buttonContainer}>
          <Button
            title={'Add Reminder'}
            onPress={() => {
              navigation.navigate('ReminderAddEdit', {date: route.params.date});
            }}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {reminder} = state;
  return {
    reminders: reminder.reminders,
  };
};

export default connect(mapStateToProps, {
  deleteAllReminders: deleteAllRemindersAction,
})(ReminderList);
