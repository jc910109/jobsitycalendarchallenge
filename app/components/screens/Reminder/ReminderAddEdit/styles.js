import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  form: {
    margin: 10,
  },
  buttonContainer: {
    paddingTop: 15,
  },
});
