import React from 'react';
import {View, ScrollView, Button} from 'react-native';
import styles from './styles';
import {
  addReminder as addReminderAction,
  editReminder as editReminderAction,
} from '../../../../redux/actions';
import {connect} from 'react-redux';
import {navigation} from '../../../../helpers/navigation';
import ReminderTextInput from './components/ReminderTextInput';
import ReminderColorPicker from './components/ReminderColorPicker';
import ReminderTimePicker from './components/ReminderTimePicker';
import ReminderDatePicker from './components/ReminderDatePicker';
import ReminderCountryCityPicker from './components/ReminderCountryCityPicker';

const initialError = {
  title: '',
  time: '',
  color: '',
  date: '',
  countryCity: '',
};

class ReminderAddEdit extends React.Component {
  constructor(props) {
    super(props);
    const {route} = props;
    const reminder = route.params.reminder;
    this.state = {
      data: {
        id: reminder ? reminder.id : null,
        title: reminder ? reminder.title : '',
        time: reminder ? reminder.time : '',
        color: reminder ? reminder.color : '',
        date: route.params.date,
        initialDate: route.params.date,
        countryCity: reminder
          ? reminder.countryCity
          : {
              country: '',
              city: '',
            },
      },
      error: {...initialError},
    };
  }

  validateForm() {
    let hasErrors = false;
    let error = {...initialError};
    const {data} = this.state;

    if (!data.title || data.title.trim().length === 0) {
      hasErrors = true;
      error.title = 'You must type a reminder';
    }
    if (!data.time) {
      hasErrors = true;
      error.time = 'You must pick a time';
    }
    if (!data.color) {
      hasErrors = true;
      error.color = 'You must pick a color';
    }
    if (!data.date) {
      hasErrors = true;
      error.date = 'You must pick a date';
    }
    if (!data.countryCity.country || !data.countryCity.city) {
      hasErrors = true;
      error.countryCity = 'You must pick a country and a city';
    }

    this.setState({
      error,
    });
    return hasErrors;
  }

  render() {
    const {data, error} = this.state;
    const {addReminder, editReminder} = this.props;
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.form}>
            <ReminderTextInput
              value={data.title}
              error={error.title}
              onChange={(newText) => {
                this.setState({
                  data: {
                    ...data,
                    title: newText,
                  },
                  error: {
                    ...error,
                    title: '',
                  },
                });
              }}
            />
            <ReminderDatePicker
              value={data.date}
              error={error.date}
              onChange={(newDate) => {
                this.setState({
                  data: {
                    ...data,
                    date: newDate,
                  },
                  error: {
                    ...error,
                    date: '',
                  },
                });
              }}
            />
            <ReminderTimePicker
              value={data.time}
              error={error.time}
              onChange={(newTime) => {
                this.setState({
                  data: {
                    ...data,
                    time: newTime,
                  },
                  error: {
                    ...error,
                    time: '',
                  },
                });
              }}
            />
            <ReminderColorPicker
              value={data.color}
              error={error.color}
              onChange={(newColor) => {
                this.setState({
                  data: {
                    ...data,
                    color: newColor,
                  },
                  error: {
                    ...error,
                    color: '',
                  },
                });
              }}
            />
            <ReminderCountryCityPicker
              value={data.countryCity}
              error={error.countryCity}
              onChange={(newCountryCity) => {
                this.setState({
                  data: {
                    ...data,
                    countryCity: newCountryCity,
                  },
                  error: {
                    ...error,
                    countryCity: '',
                  },
                });
              }}
            />
            <View style={styles.buttonContainer}>
              <Button
                title={`${data.id ? 'Update' : 'Create'} Reminder`}
                onPress={() => {
                  const hasErrors = this.validateForm();
                  if (!hasErrors) {
                    data.id ? editReminder(data) : addReminder(data);
                    navigation.goBack();
                  }
                }}
              />
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default connect(null, {
  addReminder: addReminderAction,
  editReminder: editReminderAction,
})(ReminderAddEdit);
