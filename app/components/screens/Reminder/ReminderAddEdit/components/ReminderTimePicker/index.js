import React, {useState} from 'react';
import {View, Button, Text} from 'react-native';
import styles from './styles';
import DateTimePicker from '@react-native-community/datetimepicker';
import {
  formatTime,
  getDateObject,
  formatDate,
  getCurrentDate,
} from '../../../../../../helpers/date';

const ReminderTimePicker = ({value, onChange, error}) => {
  const [open, setOpen] = useState(false);
  return (
    <View style={styles.container}>
      <Text style={styles.label}>Pick Time</Text>
      <View style={styles.row}>
        {value.length > 0 && <Text style={styles.time}>{value}</Text>}
        <View>
          <Button
            title={value ? 'Change Time' : 'Set Time'}
            onPress={() => {
              setOpen(true);
            }}
          />
        </View>
      </View>
      {open && (
        <DateTimePicker
          value={getDateObject(
            formatDate(getCurrentDate()),
            value ? value : '00:00',
          )}
          mode={'time'}
          is24Hour={true}
          display="spinner"
          onChange={(event, date) => {
            setOpen(false);
            if (date) {
              onChange(formatTime(date));
            }
          }}
        />
      )}
      {error.length > 0 && <Text style={styles.error}>{error}</Text>}
    </View>
  );
};

export default ReminderTimePicker;
