import {StyleSheet} from 'react-native';
import {Colors} from '../../../../../../config/color';
import {FontSizes} from '../../../../../../config/fontSize';

export default StyleSheet.create({
  container: {
    marginTop: 15,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  label: {
    fontSize: FontSizes.h4,
    color: Colors.black,
  },
  time: {
    fontSize: FontSizes.h3,
    color: Colors.black,
  },
  error: {
    fontSize: FontSizes.h6,
    color: Colors.red,
  },
});
