import React from 'react';
import {View, Button, Text} from 'react-native';
import styles from './styles';
import {navigation} from '../../../../../../helpers/navigation';
import {SELECT_DATE} from '../../../../Calendar/constants/modes';

const ReminderDatePicker = ({value, onChange, error}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.label}>Pick Date</Text>
      <View style={styles.row}>
        <Text style={styles.time}>{value}</Text>
        <View>
          <Button
            title={'Change Date'}
            onPress={() => {
              navigation.navigate('DatePicker', {
                mode: SELECT_DATE,
                onDateSelected: (date) => {
                  onChange(date);
                },
              });
            }}
          />
        </View>
      </View>
      {error.length > 0 && <Text style={styles.error}>{error}</Text>}
    </View>
  );
};

export default ReminderDatePicker;
