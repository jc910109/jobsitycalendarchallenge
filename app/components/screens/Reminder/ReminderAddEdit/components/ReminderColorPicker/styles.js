import {StyleSheet} from 'react-native';
import {Colors} from '../../../../../../config/color';
import {FontSizes} from '../../../../../../config/fontSize';

export default StyleSheet.create({
  container: {
    marginTop: 15,
  },
  input: {
    borderWidth: 1,
    borderColor: Colors.grey,
    fontSize: FontSizes.h3,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  colorOption: {
    width: 40,
    height: 40,
    marginRight: 3,
    marginTop: 3,
  },
  label: {
    fontSize: FontSizes.h4,
    color: Colors.black,
  },
  colorOptionSelected: {
    borderWidth: 5,
    borderColor: Colors.blue,
  },
  error: {
    fontSize: FontSizes.h6,
    color: Colors.red,
  },
});
