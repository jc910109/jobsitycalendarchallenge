import React from 'react';
import {View, Text} from 'react-native';
import styles from './styles';
import {chunk} from '../../../../../../helpers/general';
import {PickerColors} from '../../../../../../config/color';
import {getColorSquareCount} from '../../../../../../helpers/theme';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';

const ReminderColorPicker = ({value, onChange, error}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.label}>Pick a color for your reminder</Text>
      {chunk(PickerColors, getColorSquareCount()).map((row, rowIndex) => (
        <View key={rowIndex} style={styles.row}>
          {row.map((colorOption, colorOptionIndex) => (
            <TouchableWithoutFeedback
              key={colorOptionIndex}
              onPress={() => {
                onChange(colorOption.code);
              }}>
              <View
                style={[
                  styles.colorOption,
                  {backgroundColor: colorOption.code},
                  value === colorOption.code ? styles.colorOptionSelected : {},
                ]}
              />
            </TouchableWithoutFeedback>
          ))}
        </View>
      ))}
      {error.length > 0 && <Text style={styles.error}>{error}</Text>}
    </View>
  );
};

export default ReminderColorPicker;
