import React from 'react';
import {View, TextInput, Text} from 'react-native';
import styles from './styles';
import {Colors} from '../../../../../../config/color';

const ReminderTextInput = ({value, onChange, error}) => (
  <View style={styles.container}>
    <Text style={styles.label}>Reminder</Text>
    <TextInput
      style={styles.input}
      placeholder="Type your reminder"
      placeholderTextColor={Colors.grey}
      maxLength={30}
      onChangeText={(newText) => {
        onChange(newText);
      }}
      value={value}
    />
    {error.length > 0 && <Text style={styles.error}>{error}</Text>}
  </View>
);

export default ReminderTextInput;
