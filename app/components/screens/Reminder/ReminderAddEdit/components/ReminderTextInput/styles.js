import {StyleSheet} from 'react-native';
import {Colors} from '../../../../../../config/color';
import {FontSizes} from '../../../../../../config/fontSize';

export default StyleSheet.create({
  container: {
    marginTop: 15,
  },
  input: {
    borderWidth: 1,
    borderColor: Colors.grey,
    fontSize: FontSizes.h3,
  },
  label: {
    fontSize: FontSizes.h4,
    color: Colors.black,
  },
  error: {
    fontSize: FontSizes.h6,
    color: Colors.red,
  },
});
