import React from 'react';
import {View, Text} from 'react-native';
import styles from './styles';
import {Picker} from '@react-native-community/picker';
import {
  countryList,
  getCitiesFromCountry,
} from '../../../../../../helpers/general';

const ReminderCountryCityPicker = ({value, onChange, error}) => {
  return (
    <View>
      <View style={styles.container}>
        <Text style={styles.label}>Country</Text>
        <Picker
          selectedValue={value.country}
          onValueChange={(itemValue) => {
            value.country = itemValue;
            onChange(value);
          }}>
          <Picker.Item label="Pick Country" value="" />
          {countryList.map((country) => (
            <Picker.Item
              key={country.code}
              label={country.name}
              value={country.code}
            />
          ))}
        </Picker>
      </View>
      {value.country.length > 0 && (
        <View style={styles.container}>
          <Text style={styles.label}>City</Text>
          <Picker
            selectedValue={value.city}
            onValueChange={(itemValue) => {
              value.city = itemValue;
              onChange(value);
            }}>
            <Picker.Item label="Pick City" value="" />
            {getCitiesFromCountry(value.country).map((city) => (
              <Picker.Item key={city.id} label={city.name} value={city.id} />
            ))}
          </Picker>
        </View>
      )}
      {error.length > 0 && <Text style={styles.error}>{error}</Text>}
    </View>
  );
};

export default ReminderCountryCityPicker;
