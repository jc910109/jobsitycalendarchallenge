# Jobsity Calendar Challenge

Calendar developed using React Native in a bare app and prepared for testing and working on an android device (every library and code practices used are also compatible with ios devices, but it wasn't tested because i don't have a mac neither an ios phone, if it is mandatory just tell me and i'll setup a mac virtual machine with a ios simulator in order to test the app in ios).

Every mandatory and optional (bonus) feature are included and coded.

The code is linted using eslint and can be checked running

```bash
npm run lint
``` 

## Installation

React Native enviroment ca be setup using [React Native CLI Quickstart](https://reactnative.dev/docs/environment-setup) instructions.

When the enviroment is ready just connect an android phone to PC and enable [Developer Mode](https://www.howtogeek.com/129728/how-to-access-the-developer-options-menu-and-enable-usb-debugging-on-android-4.2/)

Once the mobile phone is ready just place the terminal in the root folder of the project and run

```bash
npm install
```
and then 

```bash
npm run android
```

Also is included a compiled release apk for android at stores/android/app-release.apk

## Usage

The app is very intuitive but here is a description of every screen:

* My Calendar: Show current month calendar but is able to change month and year showed. Tapping a date will navigate to reminders list page for the date tapped, also in the calendar view are showed the colors of reminders for each date.

* Reminders List: Show a scrolled list of reminders setted to date selected. A reminder can be deleted swiping rigth it and then clicking on "TAP TO DELETE" button.

* Reminder Form: Screen used to add or edit a reminder, all fields are required.

Some image examples for every screen can be found inside example_images folder

## Unit Test

Add Reminder unit test was developed using jest and are a total of 7 tests that can be checked running

```bash
npm run test
```


## Developed by
[Jose Carlos Torres Alvarez](https://www.linkedin.com/in/jc910109/)