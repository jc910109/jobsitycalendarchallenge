/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { Provider } from "react-redux";
import store from "./app/redux/store";
import Navigation from './app/routes';

GLOBAL.XMLHttpRequest = GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest;

const App: () => React$Node = () => {
    return (
        <Provider store={store}>
            <Navigation/>
        </Provider>
    );
};

export default App;
