import * as actions from '../app/redux/actions'
import * as types from '../app/redux/actionTypes'
import reducer from '../app/redux/reducers/reminder'

const firstActionContent = {
    title: 'Go to work',
    date: '2020-05-11',
    time: '08:00',
    color: 'red',
    countryCity: {
        country: 'CU',
        city: 1234,
    }
};
const secondActionContent = {
    title: 'Say hello to everybody',
    date: '2020-05-11',
    time: '09:00',
    color: 'green',
    countryCity: {
        country: 'ES',
        city: 54678,
    }
};
const thirdActionContent = {
    title: 'Have a nice meal',
    date: '2020-05-12',
    time: '13:00',
    color: 'yellow',
    countryCity: {
        country: 'EU',
        city: 854564,
    }
};

describe('reminder actions', () => {

    it('should create an action to add a reminder', () => {
        const expectedAction = {
            type: types.ADD_REMINDER,
            payload: {
                id: 1,
                content: firstActionContent
            }
        }
        expect(actions.addReminder(firstActionContent)).toEqual(expectedAction)
    })
    it('should create a second action to add a reminder', () => {
        const expectedAction = {
            type: types.ADD_REMINDER,
            payload: {
                id: 2,
                content: secondActionContent
            }
        }
        expect(actions.addReminder(secondActionContent)).toEqual(expectedAction)
    })
    it('should create a third action to add a reminder', () => {
        const expectedAction = {
            type: types.ADD_REMINDER,
            payload: {
                id: 3,
                content: thirdActionContent
            }
        }
        expect(actions.addReminder(thirdActionContent)).toEqual(expectedAction)
    })
})

describe('reminder reducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual({
            reminders: {
            }
        })
    })

    let state = undefined

    const payloadFirstReminder = {
        id: 1,
        content: firstActionContent
    };

    const payloadSecondReminder = {
        id: 2,
        content: secondActionContent
    }

    const payloadThirdReminder = {
        id: 3,
        content: thirdActionContent
    }

    it('should handle first ADD_REMINDER', () => {        
        state = reducer(state, {
            type: types.ADD_REMINDER,
            payload: payloadFirstReminder
        })
        expect(state).toEqual({
            reminders: {
                '2020-05-11': [
                    {
                        id: payloadFirstReminder.id,
                        ...payloadFirstReminder.content
                    }
                ]
            }
        })        
    })

    it('should handle second ADD_REMINDER', () => {        
        state = reducer(state, {
            type: types.ADD_REMINDER,
            payload: payloadSecondReminder
        })
        expect(state).toEqual({
            reminders: {
                '2020-05-11': [
                    {
                        id: payloadFirstReminder.id,
                        ...payloadFirstReminder.content
                    },
                    {
                        id: payloadSecondReminder.id,
                        ...payloadSecondReminder.content
                    }
                ]
            }
        })        
    })

    it('should handle third ADD_REMINDER', () => {        
        state = reducer(state, {
            type: types.ADD_REMINDER,
            payload: payloadThirdReminder
        })
        expect(state).toEqual({
            reminders: {
                '2020-05-11': [
                    {
                        id: payloadFirstReminder.id,
                        ...payloadFirstReminder.content
                    },
                    {
                        id: payloadSecondReminder.id,
                        ...payloadSecondReminder.content
                    }
                ],
                '2020-05-12': [
                    {
                        id: payloadThirdReminder.id,
                        ...payloadThirdReminder.content
                    }
                ]
            }
        })        
    })
})